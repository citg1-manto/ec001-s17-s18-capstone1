package com.zuitt.capstone.repositories;

import com.zuitt.capstone.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// Post is the data type of the data used in methods
// Object is the data type of the data returned from the database.
// An interface marked as @Repository contains methods for database manipulation

@Repository
public interface CourseRepository extends CrudRepository<Course,Object>{

}
