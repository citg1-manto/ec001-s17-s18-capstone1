// contains the business logic concerned with a particular object in the class
package com.zuitt.capstone.services;


import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }
    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    // Delete Course
    public ResponseEntity deleteCourse(Long id,String stringToken){
//       postRepository.deleteById(id);
//       return new ResponseEntity<>("Post Deleted successful" +
//               "ly", HttpStatus.OK);
//
        Course courseForDeleting = courseRepository.findById(id).get();
        String courseName = courseForDeleting.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUsername.equals(courseName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course Delete Successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this Course",HttpStatus.UNAUTHORIZED);
        }
    }

    // Update a post
    public ResponseEntity updateCourse(Long id, String stringToken, Course course){

        Course courseForUpdating = courseRepository.findById(id).get();
        // Get the "author" of the specific post.
        String courseName = courseForUpdating.getUser().getUsername();
        // Get the "username" from the StringToken to compare it with the username of the current post being edited.
        String authenticatedUserName= jwtToken.getUsernameFromToken(stringToken);


        // check if the username of the authenticated user matches the username of the post's author
        if(authenticatedUserName.equals(courseName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());
            
            courseRepository.save(courseForUpdating);
            return  new ResponseEntity("Course has been Revised.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Your are not authorize to update this Course.",HttpStatus.UNAUTHORIZED);
        }
    }
    
}