package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    // Create Course
    void createCourse(String stringToken, Course course);
    // Get Course
    Iterable<Course> getCourses();
    // Delete Course
    ResponseEntity deleteCourse(Long id, String stringToken);
    // Update a Course
    ResponseEntity updateCourse(Long id,String stringToken, Course course);

}
