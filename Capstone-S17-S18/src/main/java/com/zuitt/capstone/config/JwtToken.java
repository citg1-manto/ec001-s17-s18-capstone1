package com.zuitt.capstone.config;

import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken {
    // to get token from application.properties
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = -8654047034798646805L;

    // Time duration in seconds that the token can be used.
    public  static  final long JWT_TOKEN_VALIDITY = 5*60*60; //5 Hours

    // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
    // .setClaims includes the information to show the recipient which is the username
    // .setSubject adds information about the subject. (The subject username.)
    // .setExpiration sets the expiration of the token
    // .signWith creates the token using a declared algorithm, with the secret keyword.
    // "HS512" is a secure cryptographic algorithm.
    // The "secret key" is passed as a parameter and is used to verify the signature later on.
    // .compact() is used to generate the final JWT string by compacting the JWT builder object.

    // This generates the
    private String doGenerateToken(Map<String,Object> claims, String subject){
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY *1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    public String generateToken(UserDetails userDetails){
        Map<String,Object>claims = new HashMap<>();
        // Retrieves the user information from the userRepository by using the username from the UserDetails object.

        User user = userRepository.findByUsername(userDetails.getUsername());

        claims.put("user", user.getId());
         // "id" is  added to the claims of JWT to be used to identify the user in the server.

        return doGenerateToken(claims,userDetails.getUsername());

    }

    // Token validation by extracting the username from the token
    public Boolean validateToken(String token, UserDetails userDetails){
        // Extract the username from the token and store it in a variable
        final String username = getUsernameFromToken(token);
        // Returns a Boolean value that indicates whether the token is valid or not.
        // First conditions checks if the "username extracted from the token"
        // Second condition checks if the token is expired by invoking the "isTokenExpired()" method.
        return (username.equals(userDetails.getUsername()) && !isTokenExpire(token));
    }

    // This extract the specific claim from a JWT by using functional interface.
    public <T> T getClaimForToken(String token, Function<Claims,T>claimsResolver){
        final Claims claims = getAllClaimsFromToken(token);
        // This Extract to specific claim value.
        return claimsResolver.apply(claims);
    }

    // This extracts all the claims from the token.
    private Claims getAllClaimsFromToken(String token){
        // is used to parse a JWT token
        return Jwts.parser()
                // sets the secret key used to sign token.
                .setSigningKey(secret)
                // parseClaimsJws() parses the JWT token
                .parseClaimsJws(token)
                // contains all the claim that were included in the JWT
                .getBody();
    }

    // To extract the subject(username) from the JWT token
    public String getUsernameFromToken(String token){
        String claim = getClaimForToken(token,Claims::getSubject);
        return claim;
    }
    // This is used to extract expiration date of token.
    public Date getExpirationDateFromToken(String token){
        return getClaimForToken(token,Claims::getExpiration);
    }
    // This is used to check if the JWT token has expired.
    private boolean isTokenExpire(String token){
        final Date expiration = getExpirationDateFromToken(token);
        // This returns a boolean value that represents whether the current time is "before" the expiration date of the JWT Token (expiration)
        // True, if the current time is after the expiration
        // False, if the current time is before the expiration.
        return expiration.before(new Date());
    }
}
